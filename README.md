# ah_osconfig_auto_upgrades_debian

An Ansible role for configuring `unattended-upgrades` to automatically install security upgrades on Debian family systems (with email support).

Depends on [ah_osconfig_postfix_mta](https://gitlab.com/ah-ansible-roles/ah_osconfig_postfix_mta).

This role **does** use my custom Ansible Vault credentials as a result of depending on the above Postfix role.

## Compatibility
Tested for compatibility and idempotence against:

| **Distribution** | **Tested** (Y / N) | **Compatibility** (WORKING / TODO) |
|---|---|---|
| Raspberry Pi OS | Y | WORKING |
| Ubuntu | Y | WORKING |

## Changelog

| **Date** | **Description** |
|---|---|
| 2022-04-28 | Clearer documentation and more robust config declaration. |
| 2022-01-03 | Make sure Postfix dependency is properly declared, to enable email sending. |
| 2022-01-03 | First role commit. |
